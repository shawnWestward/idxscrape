/**
 * logic for omnibar search
 */
class omnibar{
    constructor()
    {
       this.setListeners();
       this.autocomplete();
    }

    setConfig(config)
    {
        this.config= config
    }

    fields()
    {
            $.post('https://idxomnibar.com/api/html/section/gather', {sections:this.config['fields']}, function(data) {
                    $('#omni-sections').append(data);
                }
            );
    }

    setListeners()
    {
        $('.idxForgeField[type="text"]').each(function(){
            $(this).val('');
        });
        let thisClass = this;
            $('body').on('click','a.hasType', function(){
                $('.omnibar.idxForgeField').attr('data-type', $(this).attr('data-type'));
                if($(this).attr('data-value') && $(this).attr('data-value') != 'undefined'){
                    $('.omnibar.idxForgeField').attr('data-value', $(this).attr('data-value'));
                }
            });

            $('body').on('click','.ui-menu-item', function(){

                //if subdivision selected, select sfr proptype
                if($(this).attr('data-type') == 'a_subdivision') {

                    $('.idxForgeField[value="sfr"]').prop('checked',true);
                }
                 $('.omnibar.idxForgeField').attr('data-type', $(this).attr('data-type'));
                 if($(this).attr('data-value') && $(this).attr('data-value') != 'undefined'){
                $('.omnibar.idxForgeField').attr('data-value', $(this).attr('data-value'));
            }
                });
        $('body').on('click','.idxForgeSubmit', function() {
            let data = {};
            $('.idxForgeField').each(function(){
                if($(this).attr('data-name')) {
                    if ($(this).attr('type') == 'radio' || $(this).attr('type') == 'checkbox'){
                        if($(this).prop('checked')){
                            data[$(this).attr('data-name')] = $(this).val();
                        }
                    } else {
                        let val = $(this).val();
                         if($(this).attr('data-value') && $(this).attr('data-value') != 'undefined'){
                            val = $(this).attr('data-value');
                         }
                        data[$(this).attr('data-name')] = val;
                    }

                }
            });
            let urlString = thisClass.processURL(data, $('.omnibar.idxForgeField').attr('data-type'));
            console.log(urlString);
            location.href = 'https://search.laflorida.com/idx/results/listings?'+urlString;
        });
    }

    autocomplete()
    {


        let autocompleteSetInterval = setInterval(function(){
            if ($('#tags').length != 0) {
                clearInterval(autocompleteSetInterval);
            }
            let user = $('.idxForgeHolder').attr('data-user');
            $("#tags" ).autocomplete({
                source: "https://idxomnibar.com/api/"+user+"/address",
                minLength: 2,
                select: function( event, ui ) {

                }
            }).data("ui-autocomplete")
            ._renderItem = function(ul, item) {
                $('.omnibar.idxForgeField').attr('data-type', '');
               let itemParts = item.type;
               let itemType = '';
               let itemClass = '';
               if(itemParts[1]){
                itemType = itemParts.trim().toLowerCase();

                if(itemType == 'city'){
                    itemType = 'city[]';
                }
                itemClass="hasType";
               }
               console.log(item);
                var listItem = $("<li class='ac_menuItem' data-type='"+itemType+"' data-value='"+item.passValue+"'></li>")
                    .data("item.autocomplete", item)
                    .append("<a class='"+itemClass+"' data-type='"+itemType+"' data-value='"+item.passValue+"'>" + item.label + "</a>")
                    .appendTo(ul);

                if (item.personal) {
                    listItem.addClass("personal");
                }
                return listItem;
            };
        }, 100);
    }

    // processing variables to build url
    processURL(data, type=''){
        let thisClass= this;
        let url = [];
        $.each(data, function(k,v){
            console.log(k);
            console.log(thisClass);
            if (typeof thisClass[k] === "function") {
                let stuff = '';
                if(k == 'omnibar') {
                    stuff = thisClass[k](v, type);
                } else {
                    stuff = thisClass[k](v);
                }

                if (stuff) {
                    url.push(stuff);
                }
            } else {
                let stuff = thisClass.defaultType(k,v);
                if (stuff) {
                    url.push(stuff);
                }
            }
        });

        let address = url.join('&');
        console.log(address);
        return address;
    }

// custom methods for data url translations
    omnibar(v, type) {
        console.log(type);
        if (v == '') {
            return false;
        }
        let check = /[0-9]{5}/;
        console.log(check);
        console.log(v.match(check));
        console.log(v);


        if(type != ''){
            return type+'='+v;
        }
        if (v.match(check)[0] == v.match(check).input){
            return 'ccz=zipcode&zipcode='+v;
        }
        return 'aw_address='+v;
    }

    // property type
    pt(v) {
        if (v == '' || v == null) {
            return false;
        }
        return 'pt='+v;
    }

    defaultType(k,v){

      if (v == '' || v == null  || v == 0) {
            return false;
        }
        return k+'='+v;
    }
}

//initiate omnibar
let obar = new omnibar();
