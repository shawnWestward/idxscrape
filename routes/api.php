<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\mainController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/{user}/address', [mainController::class, 'apiAddress']);
Route::get('/html/{user}', [mainController::class, 'apiHtml']);
Route::post('/html/section/gather', [mainController::class, 'apiHtmlSection']);
Route::get('/autocomplete', [mainController::class, 'apiAutocomplete']);


