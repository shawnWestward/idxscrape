<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use DB;

class mlsPropCount extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'mlscount';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
      $idxID = 'c114';
      $url = 'https://search.laflorida.com/idx/results/listings';
      $count = 0;
      $price = 0;

      $incre = 10000;
      echo "\n";
      while ($price < 50000000){
        list($price, $count, $incre) = $this->check($url, $price, $count, $incre);
        $price += $incre;




      }

// $lp = 50001;
//       $hp = 100000;
//       $incre = 50000;
//       while ($hp < 1000001){
//             list($lp, $hp, $count, $incre) = $this->check($url, $lp, $hp, $count, $incre);
//       }





// $lp = 1000000;
// $hp =100000000;

//        list($lp, $hp, $count, $incre) = $this->check($url, $lp, $hp, $count, $incre);


    }


    public function check($url, $price, $count, $incre){

        $link = $url.'?ccz=city&lp='.$price.'&hp='.($price+$incre).'&start=1&verbose&bare&per=1';
        $html = file_get_contents($link);
        preg_match_all('|totalResults] => (\d*)|', $html, $matches);

        $found = intval($matches[1][0]);

        if($found == 500){
            echo '.';

            $return = $this->check($url, $price, $count, ceil($incre/2));



            return $return;
        } else if ($found <100 && $incre < 1000000000) {
            echo '.';

            $return = $this->check($url, $price, $count, ceil($incre*1.25));
            return $return;
        } else {
            echo "\n";
            echo '                                                 ';

             echo "\n";
            echo $price."\t";
            echo $price+$incre."\t";
            echo $found; echo"\t";
            $count += $found;
            echo $count."\n";
            $link = $url.'?ccz=city&lp='.$price.'&hp='.($price+$incre).'&start=1&verbose&bare&per=100';
            $this->scrape($link);
            return [$price, $count, $incre];
        }
    }


    public function scrape($link) {
        $finalAll= [];
        $info = [];
        $done = false;
        $page = 1;

        while(!$done){
            $all =[];
            $link = $link.'&start='.$page;
            $html = file_get_contents($link);
            echo $link; echo "\n";
            $html= explode('[active]', $html);
            $html = $html[1];

            $fields = [
                '\[mlsPtID\]',
                '\[listingID\]',
                '\[address\]',
                '\[streetNumber\]',
                '\[streetName\]',
                '\[cityName\]',
                '\[state\]',
                '\[zipcode\]',
                '\[idxID\]',
             ];
            preg_match_all('/('.implode('|', $fields).')\s=>\s(.*)/', $html, $matches);
            $tttt= 0;

            foreach($matches[1] as $key=>$match){

                if ($match == '[idxID]'){
                    $tttt++;
                    if(count($info) >0){
                        $all[] = $info;
                    }
                    $info = [];
                }
                $thekey = str_replace('[', '', $match);
                $thekey = str_replace(']', '', $thekey);
                $info[$thekey] = $matches[2][$key];


            }

            echo "\n";
            echo 'allCount: '.count($all);


            if(count($all) < 99){
                $done = true;
                $finalAll = array_merge($finalAll, $all);
            } else{
                $page ++;
                 $finalAll = array_merge($finalAll, $all);
            }
            echo "\n";
            echo 'FinalCount: '.count($finalAll);

        }


        $updated = [];
        $inseted = [];
        echo "\n";
        $i = 0;
        foreach($finalAll as $info){

            if(count($info) == 0){

                echo '0';
                continue;
            }






            $data = $info;
            if(isset($data['mlsPtID'])){
                unset($data['mlsPtID']);
            }

            if(isset($data['zipcode'])){
                $data['zip'] = $data['zipcode'];
                unset($data['zipcode']);
            }
            if(isset($data['cityName'])){
                $data['city'] = $data['cityName'];
                unset($data['cityName']);
            }

            if(!isset($data['address'])){

               continue;
            }

            unset($data['streetNumber']);
            unset($data['streetName']);
            if(isset($data['idxID'])){
                $idxID = $data['idxID'];
            } else {
                $idxID = 'z000';
                $data['idxID'] = $idxID;
            }
            $data['found'] = date('Y-m-d H:i:s');



            $present = DB::table('address')->where('idxID', $data['idxID'])->where('address', $data['address'])->first();
            $i++;
            if(!isset($present->id)){

                if(DB::table('address')->insert($data)){
                    echo 'i';
                } else {
                    echo 'n';
                }
            } else {

                $updated[] = $present->id;
                if(DB::table('address')->where('id', $present->id)->update($data)){
                    echo 'u';
                } else {
                    echo 'N';
                }
            }
        }

        echo $i;


    }

}
