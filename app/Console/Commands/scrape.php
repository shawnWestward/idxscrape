<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use DB;

class scrape extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'scrape';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {

$clients = DB::table('clients')->where('activated', 1)->get()->toArray();



foreach($clients as $client){
    $idxRan=[];
        $idxIDsAvailable = DB::table('idxIDs')->get()->toArray();
        $idxIds = [];
        foreach($idxIDsAvailable as $idx){
            if(isset($idxIds[$idx->idxId])){
                $idxIds[$idx->idxId][] = $idx->client;
            } else {
                $idxIds[$idx->idxId]= [$idx->client];
            }
        }
        $base =$client->url;
        $html = file_get_contents($base.'idx/sitemap');
        preg_match_all('|<a.*?href="(.*)".*?>.*?</a>|', $html, $matches);

        $links = $matches[1];
        $page = 1;
        $listing = 1;
        $allAddress = [];
        $cities= [];
        foreach($links as $link) {

            if(is_numeric(strpos($link, 'city-'))){
                $parts = explode('-', $link);

                $found = DB::table('cities')->where('cityID', $parts[3])->where('idxID', $parts[1])->get()->first();
                if(!$found){
                    DB::table('cities')->insert(['cityname'=>$parts[2],'idxID'=>$parts[1], 'cityId'=>$parts[3]]);
                }
            }


            if (is_numeric(strpos($link, 'zipcode[]='))){
                 echo $link; echo "\n";
                echo $page."\n";
                $listing  = 1;

            while($listing >0) {
                    try{
                   list($listing, $idxID) = $this->listingParse($base,$link, $page, $allAddress, $idxIds, $client);
                   $idxRan[] = $idxID;
                   if($listing <100){
                    $page = 1;
                    break;
                   } else {
                    $page++;
                   }




                    } catch(Throwable $e){
                        echo "ERROR!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!";
                        $listing = 0;
                    }
                }



            }



        }

        dd($cities);

    }

    }

    public function listingParse($base, $link, $page, $allAddress, $idxData, $client){


        echo $base.$link.'<br>';
        // $link = explode('&', $link);
        // $link = $link[0];


        $html = file_get_contents($base.$link.'&per=100&bare&verbose&start='.$page);
        $html= explode('[active]', $html);
        $html = $html[1];



        $fields = [
            '\[mlsPtID\]',
            '\[listingID\]',
            '\[address\]',
            '\[streetNumber\]',
            '\[streetName\]',
            '\[cityName\]',
            '\[state\]',
            '\[zipcode\]',
            '\[idxID\]',
         ];
        preg_match_all('/('.implode('|', $fields).')\s=>\s(.*)/', $html, $matches);

        $all= [];
        $info = [];



        foreach($matches[1] as $key=>$match){

            if ($match == '[mlsPtID]'){
                $all[] = $info;
                $info = [];
            }
            $info[] = $matches[2][$key];

        }
          $all[] = $info;
                $info = [];

        $i =0;

        $updated = [];
        $inseted = [];
        foreach($all as $info){
            if(count($info) == 0){
                continue;
            }

            if($info[1] == 'Array'){
                return [0, $info[0]];
            }

            if(!isset($info[8])){
                continue;
            }


            $data = [];
            $data['state'] = $info[3];
            $data['city'] = $info[2];
            $data['zip'] = $info[4];
            $data['address'] = $info[5];
            $data['listingID'] = $info[1];

            $data['idxID'] = $info[8];
            $idxID = $info[8];
            $data['found'] = date('Y-m-d H:i:s');



            $present = DB::table('address')->where('idxID', $data['idxID'])->where('address', $data['address'])->first();

            if(!isset($present->id)){

                DB::table('address')->insert($data);
            } else {
                $updated[] = $present->id;
                DB::table('address')->where('id', $present->id)->update($data);
            }
        }
        echo "\n";
        echo 'updated: '. count($updated); echo "\n";
        $listing = count($all);
        $page++;



        if($listing == 2){
            $page = 1;

        }

        echo count($all)."\n";

if(!isset($idxID)){
    $idxID = 'aa000';
}
        return [$listing, $idxID];
        $addresses = [];
        $idxID = '';

        foreach($matches[0] as $key=>$messyAddress){
            $data= [];

            // dd($messyAddress);


            preg_match_all('|<span class="(.*?)">(.*?)<\/span>|', $messyAddress, $idxInfo);
            preg_match_all('|listing\/([a-z][0-9][0-9][0-9])\/([0-9]*)\/|', $messyAddress, $listingInfo);
            if (!isset($idxInfo[1][0])){
                continue;
            }

            if (isset($listingInfo[1][0])){
                $idxID = $listingInfo[1][0];
            } else {
                $idxID='aa000';
            }

            if (isset($listingInfo[2][0])){
                $listingID = $listingInfo[2][0];
            } else {
                $listingID = 0;
            }
            if(isset($idxData[$idxID])){
                if(in_array( $client->id, $idxData[$idxID])){

                } else {
                    DB::table('idxIDs')->insert(['idxID'=>$idxID,'client'=>$client->id]);
                    $idxData[$idxID] = [$client->id];

                }
            } else{
                DB::table('idxIDs')->insert(['idxID'=>$idxID,'client'=>$client->id]);
              $idxData[$idxID] = [$client->id];
            }

    $idxInfoFound = [];
foreach($idxInfo[1] as $key=>$info){
    $idxInfoFound[$info]=$idxInfo[2][$key];
}
if(isset($idxInfoFound['IDX-resultsAddressStateAbrv'])){
    $data['state'] = $idxInfoFound['IDX-resultsAddressStateAbrv'];
}
if(isset($idxInfoFound['IDX-resultsAddressZip'])){
    $data['zip'] = $idxInfoFound['IDX-resultsAddressZip'];
}
if(isset($idxInfoFound['IDX-resultsAddressCity'])){
    $data['city'] = $idxInfoFound['IDX-resultsAddressCity'];
}

$data['address'] = $idxInfoFound['IDX-resultsAddressNumber'].' '.$idxInfoFound['IDX-resultsAddressDirection'].' '.$idxInfoFound['IDX-resultsAddressName'];




            $data['listingID'] = $listingID;




            $data['idxID'] = $idxID;
            $data['found'] = date('Y-m-d H:i:s');


            $present = DB::table('address')->where('address', $data['address'])->first();

            if(!isset($present->id)){

                DB::table('address')->insert($data);
            } else {
                DB::table('address')->where('id', $present->id)->update($data);
            }

        }



        $listing = count($addresses);
        $page++;


        if($listing == 0){
            $page = 1;

        }
        echo count($addresses)."\n";
        $allAddress = array_merge($allAddress, $addresses);

        return [$listing, $idxID];
    }
}
