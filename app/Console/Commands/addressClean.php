<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use DB;

class addressClean extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'address:clean';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $addresses = DB::table('address')->get();
        $now = strtotime('now');
        echo $this->to_time_ago($now);
        echo "\n\n";
        echo count($addresses);
        echo "\n\ns";
        echo '<pre>';
        print_r(date('Y-m-d h:i:s'));
        echo '</pre>';


        foreach($addresses as $address){
            $found = strtotime($address->found);
            echo $this->to_time_ago($found);
            echo "\n";



        }
    }


    public function to_time_ago( $time ) {

        // Calculate difference between current
        // time and given timestamp in seconds
        $diff = time() - $time;

        if( $diff < 1 ) {
            return 'less than 1 second ago';
        }

        $time_rules = array (
                    12 * 30 * 24 * 60 * 60 => 'year',
                    30 * 24 * 60 * 60       => 'month',
                    24 * 60 * 60           => 'day',
                    60 * 60                   => 'hour',
                    60                       => 'minute',
                    1                       => 'second'
        );

        foreach( $time_rules as $secs => $str ) {

            $div = $diff / $secs;

            if( $div >= 1 ) {

                $t = round( $div );

                return $t . ' ' . $str .
                    ( $t > 1 ? 's' : '' ) . ' ago';
            }
        }
    }
}
