<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use DB;

class largezips extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'largezips';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $idx_ids = DB::table('address')->distinct('idxID')->select('idxID')->get()->toArray();

        foreach($idx_ids as $idxID) {
            $zips = DB::table('address')->distinct('zip')->select('zip')->where('idxID', $idxID->idxID)->get()->toArray();
            foreach($zips as $zip){

                $count = DB::table('address')->where('zip', $zip->zip)->where('idxID', $idxID->idxID)->count();

                if($count > 490){
                    echo $idxID->idxID. "\t". $zip->zip ."\t". $count."\n";
                }
            }
        }
    }

}
