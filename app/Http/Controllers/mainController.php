<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class mainController extends Controller
{
    public function index(){
        return view('index');
    }

    public function apiAddress($user, Request $request){
       $a = $request->get('term');


       $addresses = [];
       $idxs = getIdx($user);


       //city
        if(count($addresses) < 10){
            $cityaddresses = DB::table('cities')->whereIn('idxID', $idxs)->select('cityId','cityname')->where('cityname', 'like', '%'.$a.'%')->take(10)->get()->toArray();


            $cityaddresses = array_map(function($v){
                $info = (object)[];
                $info->type = 'city';
                $info->label = str_replace('&nbsp;' ,'', $v->cityname.' | CITY');
                $info->value = str_replace('&nbsp;' ,'', $v->cityname);
                $info->passValue = $v->cityId;

                 return $info;
             }, $cityaddresses);
             $addresses = array_merge($addresses, $cityaddresses);
        }

        if(count($addresses) < 10){
             $config =json_decode(file_get_contents("https://idxomnibar.com/js/".$user.".json"), true);
             foreach($config as $type => $data){

                $jsonResults = array_filter($data['data'], function($v) use ($a){
                        return (is_numeric(strpos(strtolower($v), strtolower($a))));
                });
                $jsonResults = array_map(function($v) use ($type, $data){
                    $info = (object)[];
                    $info->type = $type;
                    $info->label = str_replace('&nbsp;' ,'', $v.' | '.strtoupper($data['label']));
                    $info->value = str_replace('&nbsp;' ,'', $v);

                     return $info;
                 }, $jsonResults);
                $jsonResults = array_slice($jsonResults, 0,10-count($addresses));
                     $addresses = array_merge($addresses, $jsonResults);
             }

         }

        if(count($addresses) < 10){
            $addressesstreet =DB::table('address')->whereIn('idxID', $idxs)->where('address', 'like', '%'.$a.'%')->take(10)->get()->toArray();
            $addressesstreet = array_map(function($v){
                $data = (object)[];
                $data->id = $v->id;
                $address = str_replace('  ' ,' ', $v->address);
                $data->type='aw_address';
                $data->label = str_replace('  ' ,' ', $address);
                $data->value = str_replace('  ' ,' ', $address);
                 return $data;
             }, $addressesstreet);

             $addresses = array_merge($addresses, $addressesstreet);
         }


         if(count($addresses) < 10){
            $listingaddresses = DB::table('address')->whereIn('idxID', $idxs)->select('listingID')->where('listingID', 'like', '%'.$a.'%')->take(10)->get()->toArray();
            $listingaddresses = array_map(function($v){
                $data = (object)[];
                $data->type='listing id';
                $data->label = str_replace('&nbsp;' ,'', $v->listingID.' | LISTING ID');
                $data->value = str_replace('&nbsp;' ,'', $v->listingID);
                 return $data;
             }, $listingaddresses);
             $addresses = array_merge($addresses, $listingaddresses);
        }




        // if(count($addresses) < 10){
        //     $addresseszip =DB::table('address', 'zip')->where('zip', 'like', '%'.$a.'%')->take(10)->get()->toArray();
        //     $addresseszip = array_map(function($v){
        //         $data = (object)[];

        //         $data->label = str_replace('&nbsp;' ,'', $v->address.' | Zipcode');
        //         $data->value = str_replace('&nbsp;' ,'', $v->zip);
        //          return $data;
        //      }, $addresseszip);

        //      $addresses = array_merge($addresses, $addresseszip);
        //  }



       return json_encode($addresses);
    }

    public function apiHtml(Request $request, $user) {
        $userCount = DB::table('clients')->where('code', $user)->count();
        if(!$userCount){
            return 'invalid user';
        }
        return view('html')->with('user', $user);
    }

    public function apiHtmlSection(Request $request) {
        return view('sections')->with('sections', $request->get('sections'));
    }

    public function apiAutocomplete() {
        $addresses =DB::table('address')->select('address')->get()->toArray();
        $addresses = array_map(function($v){


        }, $addresses);

        echo json_encode($addresses);
    }
}
