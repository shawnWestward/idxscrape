<li class="omnipt" >
<select name="omniproptype" value="Single Family Residence" class="idxForgeFields" data-name="propertyType">
<option selected="" disabled="" value="">Select a type</option>
<option value="Single Family Residence">Single Family Residence</option>
<option value="Single+Family+Acreage">Single Family Acreage</option>
<option value="Single Family Waterfront">Single Family Waterfront</option>
<option value="Golf Course Community">Golf Course Community</option>
<option value="Farm">Farm</option>
<option value="Condominium">Condominium</option>
<option value="Patio Home/Villa">Patio Home/Villa</option>
<option value="Airstrip Community">Airstrip Community</option>
<option value="Manufactured Home w/Real Prop">Manufactured Home w/Real Prop</option>
 </select>
</li>