@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Dashboard') }}</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                   <!-- user logged in -->

                  <div class="col col-12">

                    <div class="col col-12 float-left">
                            <div class="col col-2 float-left text-center">
                               <b>IDX</b>
                            </div>
                            <div class="col col-2 float-left text-center">
                               <b>#</b>
                            </div>
                            <div class="col col-5 float-left text-center">
                                <b>Last Ran</b>
                            </div>
                            <div class="col col-3 float-left text-center">
                                <b>Old Count</b>
                            </div>
                        </div>

                    @foreach(idxIDs() as $idx)
                        <div class="col col-12 float-left">
                            <div class="col col-2 float-left text-center">
                                {{$idx->idxID}}&nbsp;
                            </div>
                            <div class="col col-2 float-left text-center">
                                {{addressCount($idx->idxID)}}
                            </div>
                            <div class="col col-5 float-left text-center">
                                {{lastRan($idx->idxID)}}
                            </div>
                            <div class="col col-3 float-left text-center">
                            {{addressOldCount($idx->idxID)}}
                            </div>
                        </div>
                    @endforeach
                  </div>

                <div class="col col-12 mt-3 float-left" style="margin-top:40px;">
                <div class="col col-12 float-left">
                <div class="col col-12 float-left">
                    <h4> Client List</h4>
                    </div>

                    @foreach(clients() as $client)
                    <div class="col col-12 float-left">
                        {{$client->name}} (<b>{{$client->code}}</b>)<br>
                        MLS: {{clientMLS($client->id)}}
                       </div>
                    @endforeach
                    </div>
                </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
