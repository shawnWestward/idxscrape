<div id="omni-container">
	<ul id="omni-sections">
		<li class="omni-search-container">
			<div class="omni-search" id="omni-tab1"><input id="tags" class="omnibar ui-autocomplete-input idxForgeField" data-name="omnibar" type="text" placeholder="Search a City, Street Address, Zip Code, or MLS#" autocomplete="off"></div>
		</li>
		<li class="omniexp-hplp">
			<div id="omni-expandable1">
				<input id="toggle1" type="checkbox" value="on"/>
				<label for="toggle1">Price</label>
				<div class="omni-add-options1">
					<div class="omnilp"><input class="omni-price-low idxForgeField" data-name="lp" type="text" placeholder="Min Price">
						<input class="omni-price-low idxForgeField" data-name="hp" type="text" placeholder="Max Price"></div>
					<div class="omnihp"></div>
				</div>
			</div>
		</li>
		<li class="omniexp-type">
			<div id="omni-expandable2">
				<input id="toggle2" type="checkbox" value="on"/>
				<label for="toggle2">Type</label>
				<div class="omni-add-options2">
					<label id="pt-radio1"><input type="radio" class="idxForgeField " name="omni-pt" data-name="pt" value="sfr">Residential</label>
<!--
					<input type="radio" id="pt-radio2" name="sfr" data-name="pt" value="1">
					  <label for="sfr">Single Family Homes</label>
					<input type="radio" id="pt-radio3" name="pt" data-name="pt" value="1">
					  <label for="sfr">Condominium</label>
					<input type="radio" id="pt-radio4" name="pt" data-name="pt" value="1">
					  <label for="sfr">Townhome</label>
-->
					<label id="pt-radio5"><input type="radio" class="idxForgeField " name="omni-pt" data-name="pt" value="ld">Land</label>
					<label id="pt-radio6"><input type="radio" class="idxForgeField " name="omni-pt" data-name="pt" value="com">Commercial</label>
				</div>
			</div>
		</li>
		<li class="omniexp-bedbath">
			<div id="omni-expandable3">
				<input id="toggle3" type="checkbox" value="on"/>
				<label for="toggle3">Beds / Baths</label>
				<div class="omni-add-options3">
					<select name="omnibeds" class="idxForgeField" data-name="bd">
						<option value="0" selected>Beds</option>
						<option value="1">1+</option>
						<option value="2">2+</option>
						<option value="3">3+</option>
						<option value="4">4+</option>
						<option value="5">5+</option>
						<option value="6">6+</option>
						<option value="7">7+</option>
						<option value="8">8+</option>
					</select>
					<select name="omnibaths" class="idxForgeField" data-name="tb">
						<option value="0" selected>Baths</option>
						<option value="1">1+</option>
						<option value="2">2+</option>
						<option value="3">3+</option>
						<option value="4">4+</option>
						<option value="5">5+</option>
						<option value="6">6+</option>
						<option value="7">7+</option>
						<option value="8">8+</option>
					</select>

				</div>
			</div>
		</li>
		<li class="omni-submit">
			<button class="omni-go idxForgeSubmit" >Search</button>
		</li>
	</ul>
</div>
