
class omnibar{

    constructor(config) {
       console.log(config);
       this.setConfig(config);
       this.fields();
       this.setListeners();
       this.autocomplete();
    }

    setConfig(config){
        console.log(config);
        this.config= config
    }

    fields() {

            $.post('http://idxomnibar.com/api/html/section/gather', {sections:this.config['fields']}, function(data){
                $('#omni-sections').append(data);
            });




    }



    setListeners(){
        let thisClass = this ;
        $('body').on('click','.results li', function(){
            $('.omnibar').val($(this).text());
            $('.results').html('');
        });

        $('body').on('click','.omni-go', function(){
            let data = {};

            $('.idxForgeFields').each(function(){
                data[$(this).attr('data-name')] = $(this).val();
            });

            let urlString = thisClass.processURL(data);
            console.log(urlString);
            location.href = 'https://search.laflorida.com/idx/results/listings?'+urlString;




        });




    }



    autocomplete() {

        console.log('autocomplete');


            let autocompleteSetInterval = setInterval(function(){

                if($('#tags').length != 0) {
                    clearInterval(autocompleteSetInterval);
                }
                $( "#tags" ).autocomplete({
                    source: "/api/address",
                    minLength: 2,
                    select: function( event, ui ) {
                      let item = ui.item.value;
                      $('#tags').val(item.label);
                      $('#tags').attr('data-value',item.value);

                    }
                  } );
            }, 100);


    }


    // processing variables to build url

    processURL(data){
        let thisClass= this;
        let url = [];
        $.each(data, function(k,v){
            let stuff = thisClass[k](v);
            if (stuff){
                url.push(stuff);
            }
        });

        let address = url.join('&');
        return address;
    }

    omnibar(v){
        if(v == ''){
            return false;
        }
        return 'aw_address='+v;
    }

    bed(v){
        if(v == '' || v == null || v == 0){
            return false;
        }
        return 'bd='+v;
    }
    bath(v){
        if(v == '' || v == null || v == 0){
            return false;
        }
        return 'tb='+v;
    }
    propertyType(v){
        if(v == '' || v == null){
            return false;
        }
        return 'pt='+v;
    }

    acres(v){
        if(v == '' || v == null || v == 0){
            return false;
        }
        return 'acres='+v;
    }
    hp(v){
        if(v == '' || v == null || v == 0){
            return false;
        }
        return 'hp='+v;
    }
    lp(v){
        if(v == '' || v == null || v == 0){
            return false;
        }
        return 'lp='+v;
    }


}

alert('here');
let obar = new omnibar();
console.log(obar);



